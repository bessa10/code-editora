<?php 

namespace CodeEduUser\Annotations;

use Doctrine\Common\Annotations\Reader;
use CodeEduUser\Annotations\Mapping\Controller;
use CodeEduUser\Annotations\Mapping\Action;

class PermissionReader
{
	/**
	* @var Reader
	*/
	private $reader;

	/**
	* PermissionReader constructor
	*/
	public function __construct(Reader $reader)
	{

		$this->reader = $reader;

	}

	// retornar todas as permissoes em qualquer controller que quiser
	public function getPermissions()
	{
		$controllersClasses = $this->getControllers();
		//pegar classes declaradas
		$declared = get_declared_classes();

		$permissions = [];

		foreach($declared as $className) {
			
			$rc = new \ReflectionClass($className);
			// verificar se a classe faz parte do array de controllers
			if(in_array($rc->getFileName(), $controllersClasses)) {

				$permission = $this->getPermission($className);
				
				if(count($permission)) {
					
					$permissions = array_merge($permissions, $permission);
				
				}
			}
		}

		return $permissions;

	}

	// pegar a permissao relativa a um controller
	public function getPermission($controllerClass)
	{
		$rc = new \ReflectionClass($controllerClass);
		// pegar as anotações
		/** 
		* @var Controller $controllerAnnotation
		*/
		$controllerAnnotation = $this->reader->getClassAnnotation($rc,Controller::class);
		
		$permissions = [];

		if($controllerAnnotation) {
			$permission = [
				'name' => $controllerAnnotation->name,
				'description' => $controllerAnnotation->description
			];

			$rcMethods = $rc->getMethods();

			foreach($rcMethods as $rcMethod) {
				/** 
				* @var Action $actionAnnotation
				*/
				$actionAnnotation = $this->reader->getMethodAnnotation($rcMethod, Action::class);

				if($actionAnnotation) {

					$permission['resource_name'] = $actionAnnotation->name;
					$permission['resource_description'] = $actionAnnotation->description;
					// codigo para reaproveitar a parce da linha 44 $permission
					$permissions[] = (new \ArrayIterator($permission))->getArrayCopy();
				}
			}
		}

		return $permissions;
	}

	private function getControllers()
	{
		$dirs = config('codeeduuser.controllers_annotation');
		
		$files = [];

		foreach($dirs as $dir) {
			foreach(\File::allFiles($dir) as $file) {
				//pegar o caminho completo do arquivo
				$files[] = $file->getRealPath();

				require_once $file->getRealPath();
			}
		}

		return $files;
	}

}
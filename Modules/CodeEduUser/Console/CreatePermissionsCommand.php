<?php

namespace CodeEduUser\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use CodeEduUser\Repositories\PermissionRepository;
use CodeEduUser\Annotations\PermissionReader;

class CreatePermissionsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'codeeduuser:make-permission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Criação de permissões baseado em controllers e actions';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    /**
    * @var PermissionRepository
    */
    private $repository;

    /**
    * @var PermissionReader
    */
    private $reader;

    public function __construct(PermissionRepository $repository, PermissionReader $reader)
    {
        parent::__construct();

        $this->repository = $repository;
        $this->reader = $reader;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //pegar as permissoes
        $permissions = $this->reader->getPermissions();

        foreach($permissions as $permission) {
            // só vai cadastrar se caso não existir permissão com o mesmo name
            if(!$this->existsPermission($permission)) {
                
                $this->repository->create($permission);
                // cadastra no banco (name, description, resource_name, resource_description)
            }
        }

        $this->info("<info>Permissoes carregadas</info>");
    }

    // método para verificar se existe a permissão
    private function existsPermission($permission)
    {
        $permission = $this->repository->findWhere([
            'name' => $permission['name'],
            'resource_name' => $permission['resource_name']
        ])->first();

        return $permission != null;
    }
}

<?php

namespace CodeEduUser\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use CodeEduUser\Http\Requests\RoleDeleteRequest;
use CodeEduUser\Http\Requests\RoleRequest;
use CodeEduUser\Repositories\RoleRepository;

class RolesController extends Controller
{
    
    private $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {   
        $roles = $this->repository->paginate(10);

        return view('codeeduuser::roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('codeeduuser::roles.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(RoleRequest $request)
    {
        $this->repository->create($request->all());
        // pegando a url anterior da requisição para redirecionar para a mesma    
        $url = $request->get('redirect_to', route('codeeduuser.roles.index'));
        // criando mensagens    
        $request->session()->flash('message', 'Perfil de usuário cadastrado com sucesso!');

        return redirect()->to($url);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $role = $this->repository->find($id);

        return view('codeeduuser::roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(RoleRequest $request, $id)
    {
        $this->repository->update($request->all(), $id);
        // pegando a url anterior da requisição para redirecionar para a mesma
        $url = $request->get('redirect_to', route('codeeduuser.roles.index'));

        $request->session()->flash('message', 'Perfil de usuário alterado com sucesso!');

        return redirect()->to($url);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try{

            $this->repository->delete($id);

            \Session::flash('message', 'Perfil de usuário excluído com sucesso!');
        
        }catch(QueryException $ex) {

            \Session::flash('message', 'Perfil de usuário não pode ser excluído. Ele está relacionado com outros registros.');

        }
        
        return redirect()->to(\URL::previous());
    }
}

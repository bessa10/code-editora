<?php

namespace CodeEduUser\Http\Controllers;

use Illuminate\Http\Response;
use CodeEduUser\Repositories\UserRepository;
use CodeEduUser\Http\Requests\UserSettingRequest;
use CodeEduUser\Annotations\Mapping\Controller as ControllerAnnotation;
use CodeEduUser\Annotations\Mapping\Action as ActionAnnotation;

/**
*  @ControllerAnnotation(name="users-settings", description="Administração")
*/

class UserSettingsController extends Controller
{

     /**
     * @var UserRepository
     */
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

     /**
     * Show the form for editing the specified resource.
     @ActionAnnotation(name="List", description="Ver listagem de usuários")
     * @return Response
     */
    public function edit()
    {
        $user = \Auth::user();
        //$user = $this->repository->find($id);

        return view('codeeduuser::user-settings.setting', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(UserSettingRequest $request)
    {
    	$user = \Auth::user();

        $this->repository->update($request->all(), $user->id);

        $request->session()->flash('message', 'Usuário alterado com sucesso!');

        return redirect()->route('codeeduuser.user_settings.edit');
    }



}

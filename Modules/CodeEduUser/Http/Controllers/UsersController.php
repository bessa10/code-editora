<?php

namespace CodeEduUser\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use CodeEduUser\Http\Requests\UserRequest;
use CodeEduUser\Http\Requests\UserDeleteRequest;
use CodeEduUser\Repositories\UserRepository;
use CodeEduUser\Repositories\RoleRepository;
use CodeEduUser\Annotations\Mapping\Controller as ControllerAnnotation;
use CodeEduUser\Annotations\Mapping\Action as ActionAnnotation;

/**
*  @ControllerAnnotation(name="users-admin", description="Administração")
*/
class UsersController extends Controller
{

     /**
     * @var UserRepository
     */
    protected $repository;

    /**
    * @var Role Repository
    */
    protected $roleRepository;

    public function __construct(UserRepository $repository, RoleRepository $roleRepository)
    {
        $this->repository = $repository;

        $this->roleRepository = $roleRepository;

    }


    /**
     * Display a listing of the resource.
     * @ActionAnnotation(name="List", description="Ver listagem de usuários")
     * @return Response
     */
    public function index()
    {
        $users = $this->repository->paginate(5);

        return view('codeeduuser::users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $roles = $this->roleRepository->all()->pluck('name', 'id');

        return view('codeeduuser::users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  UserRequest $request
     * @return Response
     */
    public function store(UserRequest $request)
    {   
        $this->repository->create($request->all());
        // pegando a url anterior da requisição para redirecionar para a mesma    
        $url = $request->get('redirect_to', route('codeeduuser.users.index'));
        // criando mensagens    
        $request->session()->flash('message', 'Usuário cadastrado com sucesso!');

        return redirect()->to($url);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->repository->find($id);

        $roles = $this->roleRepository->all()->pluck('name', 'id');

        return view('codeeduuser::users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(UserRequest $request, $id)
    {
        $data = $request->except(['password']);

        $this->repository->update($data, $id);
        // pegando a url anterior da requisição para redirecionar para a mesma
        $url = $request->get('redirect_to', route('codeeduuser.users.index'));

        $request->session()->flash('message', 'Usuário alterado com sucesso!');

        return redirect()->to($url);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(UserDeleteRequest $request, $id)
    {
        $this->repository->delete($id);

        \Session::flash('message', 'Usuário excluído com sucesso!');
        
        return redirect()->to(\URL::previous());
    }
}

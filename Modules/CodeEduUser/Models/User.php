<?php

namespace CodeEduUser\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Bootstrapper\Interfaces\TableInterface;
use Illuminate\Database\Eloquent\SoftDeletes;
use Collective\Html\Eloquent\FormAccessible;

class User extends Authenticatable implements TableInterface
{
    use Notifiable;

    use SoftDeletes;

    use FormAccessible;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    public function formRolesAttribute()
    {
        return $this->roles->pluck('id')->all();
    }

    public static function generatePassword($password = null)
    {
        return !$password ? bcrypt(str_random(8)) : bcrypt($password);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getTableHeaders()
    {
        return ['#', 'Nome', 'E-mail', 'Roles'];
    }

    public function getValueForHeader($header)
    {
        switch ($header) {
            case '#':
                return $this->id;
            case 'Nome':
                return $this->name;
            case 'E-mail':
                return $this->email;
            case 'Roles':
                return $this->roles->implode('name', ' | ');    

        }
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);   
    }

    // @param permitir uma colection ou string
    public function hasRole($role)
    {
        return is_string($role) ? 
            $this->roles->contains('name', $role) :
            (boolean) $role->intersect($this->roles)->count();
            //fazendo uma interseção entre as roles 
    }

    public function isAdmin()
    {
        return $this->hasRole(config('codeeduuser.acl.role_admin'));
    }
}


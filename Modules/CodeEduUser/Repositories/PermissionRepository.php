<?php

namespace CodeEduUser\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PermissionRepository
 * @package namespace CodeEduUser\Repositories;
 */
interface PermissionRepository extends RepositoryInterface
{
    //
}

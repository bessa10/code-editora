<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use CodeEduUser\Models\Role;
use CodeEduUser\Models\User;

class CreateAclData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $roleAdmin = Role::create([
            'name' => config('codeeduuser.acl.role_admin'),
            'description' => 'Papel de usuário mestre do sistema'
        ]);

        // pegando o usuário admin
        $user = User::where('email', config('codeeduuser.user_default.email'))->first();

        // criando o relacionamento com a role
        $user->roles()->save($roleAdmin);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // consultar a role
        $roleAdmin = Role::where('name', 'Admin')->first();

        // retirar a relação da role com o usuario adm
        $user = User::where('email', config('codeeduuser.user_default.email'))->first();

        $user->roles()->detach($roleAdmin->id);

        // excluir a role admin
        $roleAdmin->delete();
    }
}

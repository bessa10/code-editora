@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3>Novo Perfil de Usuário</h3>
			{!! Form::open(['route' => 'codeeduuser.roles.store', 'class' => 'form', 'method' => 'POST']) !!}
			{!! Form::hidden('redirect_to', URL::previous()) !!}
				<!-- nome -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('name', $errors) !!}
						{!! Form::label('name', 'Nome:', ['class'=>'control-label']) !!}
						{!! Form::text('name', null, ['class' => 'form-control']) !!}
						{!! Form::error('name', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				<!-- email -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('description', $errors) !!}
						{!! Form::label('description', 'Descrição:', ['class'=>'control-label']) !!}
						{!! Form::text('description', null, ['class' => 'form-control']) !!}
						{!! Form::error('description', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				<!-- botao -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('name', $errors) !!}
						{!! Button::primary('Criar perfil')->submit() !!}
					{!!Html::openFormGroup('name', $errors) !!}
				{!! Html::closeDiv() !!}
			{!! Form::close() !!}
		</div>	
	</div>	
@endsection

@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3>Editar Perfil de Usuário</h3>
			{!! Form::model($role, ['route' => ['codeeduuser.roles.update', 'role'=> $role->id], 'class' => 'form', 'method' => 'PUT']) !!}
			{!! Form::hidden('redirect_to', URL::previous()) !!}
				<!-- nome -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('name', $errors) !!}
						{!! Form::label('name', 'Nome:', ['class'=>'control-label']) !!}
						{!! Form::text('name', null, ['class' => 'form-control']) !!}
						{!! Form::error('name', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				<!-- Email -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('description', $errors) !!}
						{!! Form::label('description', 'Descrição:', ['class'=>'control-label']) !!}
						{!! Form::text('description', null, ['class' => 'form-control']) !!}
						{!! Form::error('description', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				<!-- botao -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup() !!}
						{!! Button::primary('Salvar perfil')->submit() !!}
					{!!Html::openFormGroup() !!}
				{!! Html::closeDiv() !!}
			{!! Form::close() !!}
		</div>	
	</div>	
@endsection
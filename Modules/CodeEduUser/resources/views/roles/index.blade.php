@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-4">
			<h1>Papéis de usuários</h1><br>
		</div>
		<div class="col-sm-offset-6 col-sm-2">
			{!! Button::primary('Novo papel de usuário')->asLinkTo(route('codeeduuser.roles.create')) !!}
		</div>
		<!-- FORMA MAIS PRÁTICA DE FAZER TABELA USANDO O BOOTSTRAPPER -->
		{!! 
			Table::withContents($roles->items())->striped()
				->callback('Ações', function($field, $role){
					$linkEdit = route('codeeduuser.roles.edit', ['role' => $role->id]);
					$linkDestroy = route('codeeduuser.roles.destroy', ['role' => $role->id]);
					$linkEditPermission = route('codeeduuser.roles.permissions.edit', ['role' => $role->id]);
					$deleteForm = "delete-form-{$role->id}";
					$form = Form::open(['route' =>
								['codeeduuser.roles.destroy', 'role' => $role->id],
								'method' => 'DELETE', 'id' => $deleteForm, 'style' => 'display:none']).
								Form::close();
					$anchorDestroy = Button::danger('Deletar')->asLinkTo($linkDestroy)->addAttributes([
										'onclick' => "event.preventDefault();document.getElementById(\"{$deleteForm}\").submit();"
									]);			
					return "<ul class=\"list-inline\">".
								"<li>".Button::primary('Editar')->asLinkTo($linkEdit)."</li>".
								"<li>|</li>".
								"<li>".$anchorDestroy."</li>".
								"<li>|</li>".
								"<li>".Button::success('Permissões')->asLinkTo($linkEditPermission)."</li>".
							"</ul>".
							$form;
				})
		!!}
		<!-- PAGINAÇÃO -->
		{{$roles->links()}}
	</div>
@endsection
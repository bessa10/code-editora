@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3>Editar meu perfil de usuário</h3>
			{!! Form::open(['route' => 'codeeduuser.user_settings.update', 'class' => 'form', 'method' => 'PUT']) !!}
			{!! Form::hidden('redirect_to', URL::previous()) !!}
				<!-- senha -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('password', $errors) !!}
						{!! Form::label('password', 'Senha:', ['class'=>'control-label']) !!}
						{!! Form::password('password', ['class' => 'form-control']) !!}
						{!! Form::error('password', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				<!-- confirmar senha -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup() !!}
						{!! Form::label('password_confirmation', 'Confirme a sua senha:', ['class'=>'control-label']) !!}
						{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
						{!! Form::error('password_confirmation', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				<!-- botao -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('name', $errors) !!}
						{!! Button::primary('Salvar')->submit() !!}
					{!!Html::openFormGroup('name', $errors) !!}
				{!! Html::closeDiv() !!}
			{!! Form::close() !!}
		</div>	
	</div>	
@endsection

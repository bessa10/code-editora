@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3>Novo Usuário</h3>
			{!! Form::open(['route' => 'codeeduuser.users.store', 'class' => 'form', 'method' => 'POST']) !!}
			{!! Form::hidden('redirect_to', URL::previous()) !!}
				<!-- nome -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('name', $errors) !!}
						{!! Form::label('name', 'Nome:', ['class'=>'control-label']) !!}
						{!! Form::text('name', null, ['class' => 'form-control']) !!}
						{!! Form::error('name', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				<!-- email -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('email', $errors) !!}
						{!! Form::label('email', 'E-mail:', ['class'=>'control-label']) !!}
						{!! Form::text('email', null, ['class' => 'form-control']) !!}
						{!! Form::error('email', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				<!--roles -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup(['roles', 'roles.*'], $errors) !!}
						{!! Form::label('roles[]', 'Papel de usuário:', ['class'=>'control-label']) !!}
						{!! Form::select('roles[]', $roles, null, ['class' => 'form-control', 'multiple' => true]) !!}
						{!! Form::error('roles.*', $errors) !!}
						{!! Form::error('roles', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				<!-- botao -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('name', $errors) !!}
						{!! Button::primary('Criar usuário')->submit() !!}
					{!!Html::openFormGroup('name', $errors) !!}
				{!! Html::closeDiv() !!}
			{!! Form::close() !!}
		</div>	
	</div>	
@endsection

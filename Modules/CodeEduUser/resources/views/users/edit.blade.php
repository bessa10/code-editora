@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3>Editar Usuário</h3>
			{!! Form::model($user, ['route' => ['codeeduuser.users.update', 'user'=> $user->id], 'class' => 'form', 'method' => 'PUT']) !!}
			{!! Form::hidden('redirect_to', URL::previous()) !!}
				<!-- nome -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('name', $errors) !!}
						{!! Form::label('name', 'Nome:', ['class'=>'control-label']) !!}
						{!! Form::text('name', null, ['class' => 'form-control']) !!}
						{!! Form::error('name', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				<!-- Email -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('email', $errors) !!}
						{!! Form::label('email', 'E-mail:', ['class'=>'control-label']) !!}
						{!! Form::text('email', null, ['class' => 'form-control']) !!}
						{!! Form::error('email', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				<!--roles -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('roles.*', $errors) !!}
						{!! Form::label('roles[]', 'Papel de usuário:', ['class'=>'control-label']) !!}
						{!! Form::select('roles[]', $roles, null, ['class' => 'form-control', 'multiple' => true]) !!}
						{!! Form::error('roles.*', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				<!-- botao -->
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('name', $errors) !!}
						{!! Button::primary('Salvar usuário')->submit() !!}
					{!!Html::openFormGroup('name', $errors) !!}
				{!! Html::closeDiv() !!}
			{!! Form::close() !!}
		</div>	
	</div>	
@endsection
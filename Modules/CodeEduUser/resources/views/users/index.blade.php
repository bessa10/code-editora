@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-4">
			<h1>Lista da Usuários</h1><br>
		</div>
		<div class="col-sm-offset-6 col-sm-2">
			{!! Button::primary('Novo Usuário')->asLinkTo(route('codeeduuser.users.create')) !!}
		</div>
		<!-- FORMA MAIS PRÁTICA DE FAZER TABELA USANDO O BOOTSTRAPPER -->
		{!! 
			Table::withContents($users->items())->striped()
				->callback('Ações', function($field, $user){
					$linkEdit = route('codeeduuser.users.edit', ['user' => $user->id]);
					$linkDestroy = route('codeeduuser.users.destroy', ['user' => $user->id]);
					$deleteForm = "delete-form-{$user->id}";
					$form = Form::open(['route' =>
								['codeeduuser.users.destroy', 'user' => $user->id],
								'method' => 'DELETE', 'id' => $deleteForm, 'style' => 'display:none']).
								Form::close();
					$anchorDestroy = Button::danger('Deletar')->asLinkTo($linkDestroy)->addAttributes([
										'onclick' => "event.preventDefault();document.getElementById(\"{$deleteForm}\").submit();"
									]);
					if($user->id == \Auth::user()->id) {
						$anchorDestroy->disable();
					}
											
					return "<ul class=\"list-inline\">".
								"<li>".Button::primary('Editar')->asLinkTo($linkEdit)."</li>".
								"<li>|</li>".
								"<li>".$anchorDestroy."</li>".
							"</ul>".
							$form;
				})
		!!}
		<!-- PAGINAÇÃO -->
		{{$users->links()}}
	</div>
@endsection
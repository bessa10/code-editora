<?php

namespace CodePub\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // REGISTRANDO A MARCO PARA MOSTRAR OS ERROS
        \Form::macro('error', function($field, $errors) {
            if(!str_contains($field, '.*') && $errors->has($field) || count($errors->get($field)) > 0){
                return view('errors.error_field', compact('field'));
            }
            return null;
        });

        // macro para form-group
        \Html::macro('openFormGroup', function($field = null, $errors = null){
            $result = false;

            if($field != null and $errors != null) {
                if(is_array($field)) {
                    foreach ($field as $value) {
                        if(!str_contains($value, '.*') && $errors->has($value) || count($errors->get($value)) > 0){
                            $result = true;
                            break;
                        }
                    }

                } else {
                    if(!str_contains($field, '.*') && $errors->has($field) || count($errors->get($field)) > 0) {
                            $result = true;
                    }

                }

            }

            $hasError = $result ? ' has-error' : '';

            return "<div class=\"form-group{$hasError}\">";

        });

        // macro fechar div
        \Html::macro('closeDiv', function() {
            return '</div>';
        });
        

        // macro nova linha
        \Html::macro('openLine', function() {
            return "<div class=\"row\">";
        });

        \Html::macro('colMd', function($col) {

            return "<div class=\"col-md-{$col}\">";

        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}

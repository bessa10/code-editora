@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3>Novo Livro</h3>
			{!! Form::open(['route' => 'books.store', 'class' => 'form', 'method' => 'POST']) !!}
			{!! Form::hidden('redirect_to', URL::previous()) !!}
				<!-- Titulo -->
				{!! Html::openLine() !!}
					{!! Html::colMd(10) !!}
						{!!Html::openFormGroup('title', $errors) !!}
							{!! Form::label('title', 'Título:', ['class'=>'control-label']) !!}
							{!! Form::text('title', null, ['class' => 'form-control']) !!}
							{!! Form::error('title', $errors) !!}
						{!! Html::closeDiv() !!}
					{!! Html::closeDiv() !!}	
				{!! Html::closeDiv() !!}
				<!-- Sub Titulo -->
				{!! Html::openLine() !!}
					{!! Html::colMd(10) !!}
						{!!Html::openFormGroup('subtitle', $errors) !!}
							{!! Form::label('subtitle', 'Subtítulo:', ['class'=>'control-label']) !!}
							{!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
							{!! Form::error('subtitle', $errors) !!}
						{!! Html::closeDiv() !!}
					{!! Html::closeDiv() !!}	
				{!! Html::closeDiv() !!}
				<!-- Preço -->
				{!! Html::openLine() !!}
					{!! Html::colMd(3) !!}
						{!!Html::openFormGroup('price', $errors) !!}
							{!! Form::label('price', 'Preço:', ['class'=>'control-label']) !!}
							{!! Form::text('price', null, ['class' => 'form-control']) !!}
							{!! Form::error('price', $errors) !!}
						{!! Html::closeDiv() !!}
					{!! Html::closeDiv() !!}	
				{!! Html::closeDiv() !!}
				<!-- Categoria -->
				{!! Html::openLine() !!}
					{!! Html::colMd(10) !!}
						{!!Html::openFormGroup(['categories', 'categories.*'], $errors) !!}
							{!! Form::label('categories[]', 'Categorias:', ['class'=>'control-label']) !!}
							{!! Form::select('categories[]', $categories, null, ['class' => 'form-control', 'multiple' => true]) !!}
							{!! Form::error('categories.*', $errors) !!}
							{!! Form::error('categories', $errors) !!}
						{!! Html::closeDiv() !!}
					{!! Html::closeDiv() !!}	
				{!! Html::closeDiv() !!}
				<!-- Botão submit -->
				{!! Html::openLine() !!}
					{!! Html::colMd(10) !!}
						{!!Html::openFormGroup(null, $errors) !!}
							{!! Button::primary('Criar categoria')->submit() !!}
						{!!Html::openFormGroup('name', $errors) !!}
						{!! Html::closeDiv() !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
			{!! Form::close() !!}
		</div>	
	</div>	
@endsection
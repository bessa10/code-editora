@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3>Editar Livro</h3>
			<p>{{$book->author->name}}</p>
			{!! Form::model($book, ['route' => ['books.update', 'book'=> $book->id], 'class' => 'form', 'method' => 'PUT']) !!}
			{!! Form::hidden('redirect_to', URL::previous()) !!}
				{!! Html::openLine() !!}
					{!! Html::colMd(10) !!}
						{!!Html::openFormGroup('title', $errors) !!}
							{!! Form::label('title', 'Título:', ['class'=>'control-label']) !!}
							{!! Form::text('title', null, ['class' => 'form-control']) !!}
							{!! Form::error('title', $errors) !!}
						{!! Html::closeDiv() !!}
					{!! Html::closeDiv() !!}	
				{!! Html::closeDiv() !!}
				{!! Html::openLine() !!}
					{!! Html::colMd(10) !!}
						{!!Html::openFormGroup('subtitle', $errors) !!}
							{!! Form::label('subtitle', 'Sub Título:', ['class'=>'control-label']) !!}
							{!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
							{!! Form::error('subtitle', $errors) !!}
						{!! Html::closeDiv() !!}
					{!! Html::closeDiv() !!}	
				{!! Html::closeDiv() !!}
				{!! Html::openLine() !!}
					{!! Html::colMd(2) !!}
						{!!Html::openFormGroup('price', $errors) !!}
							{!! Form::label('price', 'Preço:', ['class'=>'control-label']) !!}
							{!! Form::text('price', null, ['class' => 'form-control']) !!}
							{!! Form::error('price', $errors) !!}
						{!! Html::closeDiv() !!}
					{!! Html::closeDiv() !!}	
				{!! Html::closeDiv() !!}
				<!-- Categoria -->
				{!! Html::openLine() !!}
					{!! Html::colMd(10) !!}
						{!!Html::openFormGroup(['categories', 'categories.*'], $errors) !!}
							{!! Form::label('categories[]', 'Categorias:', ['class'=>'control-label']) !!}
							{!! Form::select('categories[]', $categories, null, ['class' => 'form-control', 'multiple' => true]) !!}
							{!! Form::error('categories.*', $errors) !!}
							{!! Form::error('categories', $errors) !!}
						{!! Html::closeDiv() !!}
					{!! Html::closeDiv() !!}	
				{!! Html::closeDiv() !!}
				{!! Html::openLine() !!}
					{!! Html::colMd(2) !!}
						{!!Html::openFormGroup(null, $errors) !!}
							{!! Button::primary('Salvar Livro')->submit() !!}
						{!!Html::closeDiv() !!}
					{!!Html::closeDiv() !!}	
				{!! Html::closeDiv() !!}
			{!! Form::close() !!}
		</div>	
	</div>	
@endsection
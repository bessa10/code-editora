@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3>Editar Categoria</h3>
			{!! Form::model($category, ['route' => ['categories.update', 'category'=> $category->id], 'class' => 'form', 'method' => 'PUT']) !!}
			{!! Form::hidden('redirect_to', URL::previous()) !!}
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('name', $errors) !!}
						{!! Form::label('name', 'Nome:', ['class'=>'control-label']) !!}
						{!! Form::text('name', null, ['class' => 'form-control']) !!}
						{!! Form::error('name', $errors) !!}
					{!! Html::closeDiv() !!}
				{!! Html::closeDiv() !!}
				{!! Html::openLine() !!}
					{!!Html::openFormGroup('name', $errors) !!}
						{!! Button::primary('Salvar categoria')->submit() !!}
					{!!Html::openFormGroup('name', $errors) !!}
				{!! Html::closeDiv() !!}
			{!! Form::close() !!}
		</div>	
	</div>	
@endsection
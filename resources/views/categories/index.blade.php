@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-4">
			<h1>Lista da Categorias</h1><br>
		</div>
		<div class="col-sm-offset-6 col-sm-2">
			{!! Button::primary('Nova Categoria')->asLinkTo(route('categories.create')) !!}
		</div>
		<!-- FORMA MAIS PRÁTICA DE FAZER TABELA USANDO O BOOTSTRAPPER -->
		{!! 
			Table::withContents($categories->items())->striped()
				->callback('Ações', function($field, $category){
					$linkEdit = route('categories.edit', ['category' => $category->id]);
					$linkDestroy = route('categories.destroy', ['category' => $category->id]);
					$deleteForm2 = "delete-form2-{$category->id}";
					$form = Form::open(['route' =>
								['categories.destroy', 'category' => $category->id],
								'method' => 'DELETE', 'id' => $deleteForm2, 'style' => 'display:none']).
								Form::close();
					$anchorDestroy = Button::danger('Deletar')->asLinkTo($linkDestroy)->addAttributes([
										'onclick' => "event.preventDefault();document.getElementById(\"{$deleteForm2}\").submit();"
									]);			
					return "<ul class=\"list-inline\">".
								"<li>".Button::primary('Editar')->asLinkTo($linkEdit)."</li>".
								"<li>|</li>".
								"<li>".$anchorDestroy."</li>".
							"</ul>".
							$form;
				})
		!!}
		<!-- TABELA USANDO O BLADE E HTML
		<table class="table">
			<tr>
				<th>ID</th>
				<th>Nome</th>
				<th class="text-center">Ações</th>
			</tr>
			@foreach($categories as $category)
			<tr>
				<td>{{ $category->id }}</td>
				<td>{{ $category->name }}</td>
				<td class="text-center">
					<ul class="list-inline">
						<li>
							<a href="{{route('categories.edit', ['category' => $category->id])}}" class="btn btn-success">Editar</a>
						</li>
						<li>|</li>
						<li>		
							<?php $deleteForm = "delete-form-{$loop->index}"; ?> 
							<a href="{{route('categories.destroy', ['category' => $category->id])}}" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('{{$deleteForm}}').submit();">Excluir</a>
							{!! Form::open(['route' => ['categories.destroy', 'category' => $category->id], 'method' => 'DELETE', 'id' => $deleteForm, 'style' => 'display:none']) !!}
							{!! Form::close() !!}
						</li>
					</ul>		
				</td>
			</tr>
			@endforeach
		</table> -->
		<!-- PAGINAÇÃO -->
		{{$categories->links()}}
	</div>
@endsection